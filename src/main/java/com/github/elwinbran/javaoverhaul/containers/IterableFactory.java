/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.containers;

/**
 * An object that constructs a few types of {@link java.lang.Iterable iterables}.
 * 
 * @author Elwin Slokker
 */
public interface IterableFactory
{
    /**
     * Wraps an array into an iterable.
     * 
     * @param <ElementT>
     * @param array The array to wrap. The array should not be changed after
     * using it in this method, that would break the iterable immutability.
     * @return An iterable containing only the given array.
     */
    public abstract <ElementT> Iterable<ElementT> makeFromArray(ElementT[] array);
    
    /**
     * Makes an iterable that only contains one element.
     * 
     * @param <ElementT>
     * @param soleElement The only element inside the iterable.
     * @return An iterable only containing the given element.
     */
    public abstract <ElementT> Iterable<ElementT> singleElementIterable(
            ElementT soleElement);
    
    /**
     * Grants an iterable that contains no elements.
     * 
     * @param <ElementT>
     * @return An iterable that is completely empty.
     */
    public abstract <ElementT> Iterable<ElementT> emptyIterable();
    
    /**
     * Makes an iterable that iterates the given primitive byte array.
     * For memory optimization.
     * 
     * @param array The primitive byte array to wrap. The array should not be 
     * changed after  using it in this method, that would break the iterable 
     * immutability.
     * @return An iterable containing only the given array.
     */
    public abstract Iterable<Byte> makeFromArray(byte[] array);
    
    /**
     * Makes an iterable that iterates the given primitive short array.
     * For memory optimization.
     * 
     * @param array The primitive short array to wrap. The array should not be 
     * changed after  using it in this method, that would break the iterable 
     * immutability.
     * @return An iterable containing only the given array.
     */
    public abstract Iterable<Short> makeFromArray(short[] array);
    
    /**
     * Makes an iterable that iterates the given primitive char array.
     * For memory optimization.
     * 
     * @param array The primitive char array to wrap. The array should not be 
     * changed after  using it in this method, that would break the iterable 
     * immutability.
     * @return An iterable containing only the given array.
     */
    public abstract Iterable<Character> makeFromArray(char[] array);
    
    //reverse method?
}
