/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.containers;

import com.github.elwinbran.javaoverhaul.base.NullableWrapper;

/**
 * An object that constructs a few types of 
 * {@link com.github.elwinbran.javaoverhaul.containers.Container containers}.
 * 
 * @author Elwin Slokker
 * @deprecated Container was not completely maintainable and encouraged bad practices.
 */
@Deprecated
public interface ContainerFactory
{
    /**
     * Wraps an array into a container.
     * 
     * @param <ElementT>
     * @param array The array to wrap. The array should not be changed after
     * using it in this method, that would break the iterable immutability.
     * @return A container containing only the given array.
     */
    public abstract <ElementT> Iterable<ElementT> make(ElementT[] array);
    
    /**
     * 
     * 
     * @param <ElementT>
     * @param sequence
     * @return 
     */
    public abstract <ElementT>  Iterable<ElementT> make(
            Iterable<? extends ElementT> sequence);
    
    /**
     * Takes the keys from a table and presents them as a container.
     * @param <ElementT>
     * @param table
     * @return 
     */
    public abstract <ElementT> Iterable<ElementT> make(
            Table<? extends ElementT, ?> table);
    
    /**
     * Creates container containing a single element.
     * 
     * @param <ElementT>
     * @param soleElement The element to be contained.
     * @return a container containing only the given element.
     */
    public abstract <ElementT> Iterable<ElementT> makeFromElement(
            ElementT soleElement);
    
    /**
     * Produces a container from all the values of a table.
     * 
     * @param <ElementT>
     * @param table
     * @return 
     */
    public abstract <ElementT> Iterable<NullableWrapper<ElementT>> 
        makeFromTableValues(Table<Object, ElementT> table);
    
    /**
     * Produces an empty container.
     * 
     * @param <ElementT> The required element type.
     * @return A container devoid of elements.
     */
    public abstract <ElementT> Iterable<ElementT> emptyContainer();
}
