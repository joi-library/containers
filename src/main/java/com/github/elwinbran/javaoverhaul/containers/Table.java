/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.containers;

import com.github.elwinbran.javaoverhaul.base.Equivalator;
import com.github.elwinbran.javaoverhaul.base.NullableWrapper;
import com.github.elwinbran.javaoverhaul.numbers.PositiveNumber;

/**
 * A table-like object that stores pairs of keys and values.
 * NOTE: keys are always unique within a single table object!
 * It should be immutable and implementations may choose to store null-values
 * but it is highly discouraged. Tables may use any kind of searching 
 * algorithm to retrieve key-values.
 * 
 * @author Elwin Slokker
 * @param <KeyT> The 'key' type of this table.
 * @param <ValueT> The 'value' type of this table. Defines what belongs to a 
 * key.
 */
public interface Table<KeyT, ValueT>
{
    /**
     * @return The amount of pairs this table stores.
     */
    public abstract PositiveNumber pairs();
    
    /**
     * Retrieves the value that belongs to the given {@code key} from this
     * table.
     * 
     * @param key The key of the value that needs to be looked up.
     * @param checker An object that examines whether the keys are equal.
     * @return A nullable value.
     */
    public abstract NullableWrapper<ValueT> lookup(KeyT key, Equivalator<KeyT> checker);
    
    /**
     * Checks whether the given {@code key} exists in this table.
     * 
     * @param key The key to be checked.
     * @param checker An object that examines whether the keys are equal.
     * @return {@code true} if the given key does indeed have an entry, 
     * {@code false} otherwise.
     */
    public abstract boolean checkKey(KeyT key, Equivalator<KeyT> checker);
    
    /**
     * @return all the keys in this table as immutable sequence.
     */
    public abstract Iterable<KeyT> getKeys();
}
