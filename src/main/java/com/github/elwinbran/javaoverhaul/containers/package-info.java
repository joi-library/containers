/**
 * I DO NOT CLAIM ANY OWNERSHIP OVER THE ADT's (Abstract Data Types) AS IDEAS.
 * A complete overhaul of the java collection system.
 * 
 * When reading data, I think there are only two types of retrieval required:
 * <br>-Sequenced, represented by {@link Container}.
 * <br>-Key based, represented by {@link Table}.
 * <br><br>
 * This is based on software models and real-life examples.
 * 
 * These interfaces require implementations to be immutable and thus
 * only useful for reading data. 
 * 
 * 
 * FACTORIES:
 */
package com.github.elwinbran.javaoverhaul.containers;