/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.containers;

/**
 * An abstract factory that produces the factories that produce 
 * {@link java.lang.Iterable iterables}, 
 * {@link com.github.elwinbran.javaoverhaul.containers.Container containers} and 
 * {@link com.github.elwinbran.javaoverhaul.containers.Table tables}.
 * 
 * @author Elwin Slokker
 * @deprecated review usability.
 */
@Deprecated
public interface AbstractStoreFactory
{
    /**
     * @return an object that creates iterables.
     */
    public abstract IterableFactory makeIterableFactory();
    
    /**
     * @return an object that creates containers.
     */
    public abstract ContainerFactory makeContainerFactory();
    
    /**
     * @return an object that creates tables.
     */
    public abstract TableFactory makeTableFactory();
}
