/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.containers;

import com.github.elwinbran.javaoverhaul.base.Pair;

/**
 * An object that constructs a few types of 
 * {@link com.github.elwinbran.javaoverhaul.containers.Table tables}.
 * 
 * @author Elwin Slokker
 */
public interface TableFactory
{
    /**
     * Produces a table with a single given pair.
     * 
     * @param <KeyT> The type of keys in the table.
     * @param <ValueT> The type of values in the table.
     * @param pair The pair that will form the table.
     * @return a table containing only {@code pair}.
     */
    public abstract <KeyT, ValueT> Table<KeyT, ValueT> makeFromPair(
            Pair<KeyT, ValueT> pair);
    
    /**
     * Produces a table based on given pairs.
     * 
     * @param <KeyT> The type of keys in the table.
     * @param <ValueT> The type of values in the table.
     * @param pairs All the pairs that will appear in the produced table.
     * @return a table containing all {@code pairs}.
     */
    public abstract <KeyT, ValueT> Table<KeyT, ValueT> makeFromPairs(
            Iterable<Pair<KeyT, ValueT>> pairs);
    
    /**
     * Produces a table given an iterable of 'keys' and one of 'values'.
     * The iterables should contain the same amount of items.
     * The pairs in the table will be formed according to the order of the
     * elements in the iterables.
     * 
     * @param <KeyT> The type of keys in the table.
     * @param <ValueT> The type of values in the table.
     * @param keys The keys to appear in the table. Must contain only 
     * unique values in addition to having the same number of elements as 
     * {@code values}.
     * @param values The values to appear in the table.
     * @return A table that has pairs based on the ordering of the given iterables.
     */
    public abstract <KeyT, ValueT> Table<KeyT, ValueT> makeFromSequences(
            Iterable<? extends KeyT> keys, Iterable<? extends ValueT> values);
    
    /**
     * Produces an empty table.
     * 
     * @param <KeyT> The type of keys in the table.
     * @param <ValueT> The type of values in the table.
     * @return a table with no pairs.
     */
    public abstract <KeyT, ValueT> Table<KeyT, ValueT> emptyTable();
}
